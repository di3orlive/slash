'use strict';
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'facebook', 'googleplus']);

app.filter('trusted', ['$sce', function($sce) {
    var div = document.createElement('div');
    return function(text) {
        div.innerHTML = text;
        return $sce.trustAsHtml(div.textContent);
    };
}]);

app.config(['FacebookProvider', 'GooglePlusProvider', function (FacebookProvider, GooglePlusProvider) {
    //FacebookProvider.init('1788004364764805');
    FacebookProvider.init('480259092183316');

    GooglePlusProvider.init({
        clientId: '379520381678-gcbj8o3d05p8b3hut23fa9cu81cf3obd.apps.googleusercontent.com',
        apiKey: 'VM1c_3h8WIHEmESWWKJJF8zV'
    });
}]);


app.controller('mainCtrl', [
    '$scope', 'Facebook', 'GooglePlus', '$http',
    '$timeout', '$interval', '$window',
    function ($scope, Facebook, GooglePlus, $http, $timeout,
              $interval, $window) {

//LOGIN FB&G+///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.user = {
        fb_id: '',
        fb_name: '',
        g_id: '',
        g_name: ''
    };

//getQuestions//////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getQuerystringParamValue(param) {
            var queryStr = window.location.search.substring(1);
            var queryArr = queryStr.split("&");
            for (var i=0; i<queryArr.length; i++) {
                var key = queryArr[i].split("=")[0];
                var val = queryArr[i].split("=")[1];
                if (key == param) {
                    return val;
                }
            }
            return "";
        }



    //check page url
    var isMobile = window.location.href.match(/mobile.html/g) == "mobile.html";

    $scope.questions = [];
    $scope.not_authorized_fb = false;
    $scope.getQuestions = function() {
        $http.jsonp("http://www.slash.co.il/apps/1/hamerotz_exotic_moment/records/questions/?items_in_page=5&order=random&condition=active_questions&callback=JSON_CALLBACK").success(function(data){
            $scope.questions = data.result.items;

            if(isMobile) {
                if(getQuerystringParamValue('login') === 'fb'){
                    Facebook.getLoginStatus(function(response) {
                        if (response.status === 'connected') {
                            $scope.user.fb_id = response.authResponse.userID;
                            $scope.getMeF();
                        } else if (response.status === 'not_authorized') {
                            $scope.not_authorized_fb = true;
                        }


                        if(!$scope.not_authorized_fb){
                            $scope.getMeF();
                            $scope.setTab(2);
                            $scope.startGame();
                        }
                    });
                }else if(getQuerystringParamValue('prompt') === 'none'){
                    GooglePlus.login().then(function (authResult) {
                        GooglePlus.getUser().then(function (user) {
                            console.log(user);
                            $scope.user.g_id = user.id;
                            $scope.user.g_name = user.name;
                            $scope.setTab(2);
                            $scope.startGame();
                        });
                    }, function (err) {console.log(err);});
                }
            }
        });
    };
    $scope.getQuestions();


//login/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.$watch(function() {
    return Facebook.isReady();
}, function(newVal) {
    if (newVal) {
        //$scope.facebookReady = true;
    }
});

    $scope.loginF = function() {
        cooladata.trackEvent('facebook_login_button', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});
        if(isMobile) {
            ga('send', 'event', 'mobile', 'facebook_login_button', '');
        }else {
            ga('send', 'event', 'desktop', 'facebook_login_button', '');
        }

        if(isMobile) {
            window.location.href = 'https://www.facebook.com/dialog/oauth?client_id=480259092183316&redirect_uri=http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/mobile.html?login=fb&scope=';
        }else{
            Facebook.login(function(response) {
                if (response.status == 'connected') {
                    $scope.user.fb_id = response.authResponse.userID;
                    $scope.getMeF();
                    $scope.setTab(2);
                    $scope.startGame();
                }
            }, {scope: ''});
        }
    };

    $scope.getMeF = function() {
        Facebook.api("/" + $scope.user.fb_id, function (response) {
                if (response && !response.error) {
                    $scope.user.fb_name = response.name;
                }
            }
        );
    };







    $scope.loginG = function () {
        cooladata.trackEvent('google_login_button', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});
        if(isMobile) {
            ga('send', 'event', 'mobile', 'google_login_button', '');
        }else {
            ga('send', 'event', 'desktop', 'google_login_button', '');
        }

        //if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        if(isMobile) {
            window.location.href = 'https://accounts.google.com/o/oauth2/auth?redirect_uri=http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/mobile.html&response_type=code&client_id=379520381678-6veb2934cnv2fhjra0c7aisa6j5f6fve.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/plus.login';

            //window.location.href = 'https://accounts.google.com/o/oauth2/auth?redirect_uri=http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/mobile.html?login=fb&response_type=code&client_id=379520381678-6veb2934cnv2fhjra0c7aisa6j5f6fve.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/plus.login';
        }else{
            GooglePlus.login().then(function (authResult) {
                GooglePlus.getUser().then(function (user) {
                    console.log(user);
                    $scope.user.g_id = user.id;
                    $scope.user.g_name = user.name;
                    $scope.setTab(2);
                    $scope.startGame();
                });
            }, function (err) {console.log(err);});
        }
    };


//TABS//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.tab = 1;

    $scope.setTab = function (newTab) {
        $scope.tab = newTab;

        function closeOrNot(e) {
            if($scope.checkResponseStatus){
                if(!e) e = window.event;
                e.cancelBubble = true;
                e.returnValue = 'Are you sure you want to exit?\nAttention: Data not will be saved!\n';
                if (e.stopPropagation) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
        }
        if(newTab >= 2){
            cooladata.trackEvent('page_load', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});
            if(isMobile) {
                ga('send', 'event', 'mobile', 'page_load', '');
            }else {
                ga('send', 'event', 'desktop', 'page_load', '');
            }
            window.onbeforeunload=closeOrNot;
        }
    };
    $scope.isTab = function (tab) {
        return $scope.tab === tab;
    };


//FOR CHECK USER SELECT_CHECK///////////////////////////////////////////////////////////////////////////////////////////
    $scope.touched = false;


    $scope.startRedCounter = false;
    $scope.startGame = function(){
        if($scope.questionNumber >= 5){
            $scope.stopCounter();
            $scope.setTab(3);
            return;
        }


        $scope.thisQuestion = $scope.questions[$scope.questionNumber];
        $scope.onlyQuestions = [
            {id: 1,question: $scope.thisQuestion.question_answer_1},
            {id: 2,question: $scope.thisQuestion.question_answer_2},
            {id: 3,question: $scope.thisQuestion.question_answer_3},
            {id: 4,question: $scope.thisQuestion.question_answer_4}
        ];
        function shuffle(o){
            for(var j, x, k = o.length; k; j = Math.floor(Math.random() * k), x = o[--k], o[k] = o[j], o[j] = x);
            return o;
        }
        $scope.onlyQuestions = shuffle($scope.onlyQuestions);



        $scope.timer = 0;
        $scope.timer = 8;
        $scope.questionNumber ++;
        $scope.startRedCounter = false;
        $scope.startCounter();
        $scope.timerSeconds = new Date();
    };



//timer/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var timer;
    $scope.timer = 0;
    $scope.questionNumber = 0;
    $scope.timerSeconds = null;
    $scope.stopCounter = function() {
        $timeout.cancel(timer);
    };
    $scope.startCounter = function() {
        $scope.timer--;

        if($scope.timer <= 0){
            $scope.addNewSingleAnswer();
            return;
        }

        timer = $timeout($scope.startCounter, 1000);
    };


    $scope.answerRadio = 0;
    $scope.resetAll = function() {
        $scope.answerRadio = 0;
        $scope.stopCounter();


        $timeout(function(){
            $scope.onlyQuestions = null;
            $timeout(function(){
                $scope.startGame();
            }, 0);
        }, 2000);
    };
//addNewSingleAnswer////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.score = 0;
    $scope.total_correct_answers = 0;
    $scope.total_correct_answers_time = 0;
    $scope.total_answer_time = 0;


    var buildSendData = function(){
        return {
            answer_user_id: $scope.user.fb_id ? 'fb_' + $scope.user.fb_id : $scope.user.g_id ? 'g_' + $scope.user.g_id : '',
            answer_user_fb_uid: $scope.user.fb_id || '',
            answer_user_fb_name: $scope.user.fb_name || '',
            answer_user_google_uid: $scope.user.g_id || '',
            answer_user_google_name: $scope.user.g_name || ''
        };
    };


    $scope.addNewSingleAnswer = function(item) {
        $scope.startRedCounter = true;

        if(!item){
            item = {id: null,question:null}
        }

        var sendData = buildSendData();
        var getDiffTime = +((Math.abs($scope.timerSeconds - new Date())/10000).toFixed(3) * 10).toFixed(3);
        sendData.answer_question_id = $scope.thisQuestion.id;
        sendData.answer_answer_number = item.id;
        sendData.answer_answer_time = getDiffTime;
        sendData.answer_answer_number= item.id;

        //CHECK_ANSWER//////////////////////////////////////////////////////////////////////////////////////////////////
        if(item.id == 1){
            $scope.score += 10;
            $scope.total_correct_answers += 1;
            $scope.total_correct_answers_time += getDiffTime;
        }else{
            $scope.score -= 5;
        }


        if($scope.score == 50){
            $scope.score += 15;
        }

        //TOTAL_ANSWER_TIME/////////////////////////////////////////////////////////////////////////////////////////////
        $scope.total_answer_time += getDiffTime;


        //console.log(sendData);
        $scope.resetAll();



        var sendDataUrl = "http://www.slash.co.il/apps/1/hamerotz_exotic_moment/records/answers/?method=insert" +
            "&answer_user_id=" + encodeURIComponent(sendData.answer_user_id) +
            "&answer_user_fb_uid=" + encodeURIComponent(sendData.answer_user_fb_uid) +
            "&answer_user_fb_name=" + encodeURIComponent(sendData.answer_user_fb_name) +
            "&answer_user_google_uid=" + encodeURIComponent(sendData.answer_user_google_uid) +
            "&answer_user_google_name=" + encodeURIComponent(sendData.answer_user_google_name) +
            "&answer_question_id=" + encodeURIComponent(sendData.answer_question_id) +
            "&answer_answer_number=" + encodeURIComponent(sendData.answer_answer_number) +
            "&answer_answer_time=" + encodeURIComponent(sendData.answer_answer_time) +
            "&callback=JSON_CALLBACK";


        $http.jsonp(sendDataUrl).success(function(data){
            console.log(data);
        });
    };

//sendAnswersSummery////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.sendAnswersSummery = function(sendForm, $event) {
        cooladata.trackEvent('send_button', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});


        var sendData = buildSendData();
        sendData.summery_correct_answers = $scope.total_correct_answers;
        sendData.summery_answers_time = $scope.total_answer_time;
        sendData.summery_correct_answers_time = $scope.total_correct_answers_time;
        sendData.summery_score = $scope.score;
        sendData.summery_user_full_name = sendForm.user_full_name.$modelValue;
        sendData.summery_user_phone = sendForm.user_phone.$modelValue;
        sendData.summery_user_email = sendForm.user_mail.$modelValue;


        var tihsElem = angular.element($event.currentTarget);
        tihsElem.addClass('send');
        $timeout(function(){
            tihsElem.addClass('playAgain');
        },2000);


        console.log(sendData);


        var sendDataUrl = "http://www.slash.co.il/apps/1/hamerotz_exotic_moment/records/answers_summery/?method=insert" +
            "&summery_user_id=" + encodeURIComponent(sendData.answer_user_id) +
            "&summery_user_fb_uid=" + encodeURIComponent(sendData.answer_user_fb_uid) +
            "&summery_user_fb_name=" + encodeURIComponent(sendData.answer_user_fb_name) +
            "&summery_user_google_uid=" + encodeURIComponent(sendData.answer_user_google_uid) +
            "&summery_user_google_name=" + encodeURIComponent(sendData.answer_user_google_name) +
            "&summery_correct_answers=" + encodeURIComponent(sendData.summery_correct_answers) +
            "&summery_answers_time=" + encodeURIComponent(sendData.summery_answers_time) +
            "&summery_correct_answers_time=" + encodeURIComponent(sendData.summery_correct_answers_time) +
            "&summery_score=" + encodeURIComponent(sendData.summery_score) +
            "&summery_user_full_name=" + encodeURIComponent(sendData.summery_user_full_name) +
            "&summery_user_phone=" + encodeURIComponent(sendData.summery_user_phone) +
            "&summery_user_email=" + encodeURIComponent(sendData.summery_user_email) +
            "&callback=JSON_CALLBACK";


        $http.jsonp(sendDataUrl).success(function(data){
            //console.log(data);
        });
    };




//trackingEvents////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.checkTypingInputs = function(sendForm) {
            var fullnameLength = sendForm.user_full_name ? sendForm.user_full_name.length : 0;
            var phoneLength = sendForm.user_phone ? sendForm.user_phone.length : 0;
            var emailLength = sendForm.user_mail ? sendForm.user_mail.length : 0;
            if(sendForm.user_full_name != '' && fullnameLength >= 1 && fullnameLength < 2){
                cooladata.trackEvent('typing_details_form', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});

                if(isMobile) {
                    ga('send', 'event', 'mobile', 'typing_details_form', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_details_form', '');
                }
            }
            else if(sendForm.user_phone != '' && phoneLength >= 1 && phoneLength < 2){
                cooladata.trackEvent('typing_details_form', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});

                if(isMobile) {
                    ga('send', 'event', 'mobile', 'typing_details_form', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_details_form', '');
                }
            }
            else if(sendForm.user_mail != '' && emailLength >= 1 && emailLength < 2){
                cooladata.trackEvent('typing_details_form', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});

                if(isMobile) {
                    ga('send', 'event', 'mobile', 'typing_details_form', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_details_form', '');
                }
            }
        };

    cooladata.trackEvent('page_load', {'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_spring_exsotic/app/'});
    if(isMobile) {
        ga('send', 'event', 'mobile', 'page_load', '');
    }else {
        ga('send', 'event', 'desktop', 'page_load', '');
    }

}]);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
app.directive('preLoader', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            angular.element(document).ready(function () {
                $timeout(function () {
                    element.addClass('hide');
                    angular.element(document.querySelector('.app')).removeClass('ovh');
                }, 500);
            });
        }
    };
});
