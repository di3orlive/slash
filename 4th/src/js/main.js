'use strict';
var app = angular.module('app', ['ngRoute', 'ngAnimate']);

app.controller('mainCtrl', ['$scope', function ($scope) {
    $scope.m = {
        tab: 1
    };

    $scope.setTab = function (newTab) {
        $scope.m.tab = newTab;
    };
    $scope.isTab = function (tab) {
        return $scope.m.tab === tab;
    };
}]);