'use strict';
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ezfb']);

app.filter('firstWord', function() {
    return function(input) {
        if(input == undefined || input == ""){return;}

        var firstWord = input.split(" ");
        return firstWord[0];
    }
});


//app.filter('changeImageSize', function() {
//    return function(input) {
//        if(input == undefined || input == ""){return;}
//
//        return input.replace(/p50x50/g, "p250x250");
//    }
//});


app.config(['ezfbProvider', function (ezfbProvider) {
    //FacebookProvider.init('155402328189596');
    ezfbProvider.setInitParams({
        appId: '1788004364764805'
    });
}]);



app.controller('mainCtrl', [
    '$scope',
    '$timeout',
    'ezfb',
    '$window',
    '$http',
    '$filter',
    function($scope, $timeout, ezfb, $window, $http, $filter) {
        $scope.user = {
            name: '',
            uid: '' || 10102454385528521
        };
        $scope.user_friend = {
            name: '',
            uid: '' || 10102454385528521
        };
        $scope.userToken = '';
        $scope.sharedText = '';
        $scope.logged = false;



        /**
         * TABS
         */
        $scope.tab = 1;
        $scope.setTab = function (newTab) {
            $scope.tab = newTab;

            if(newTab === 2){
                cooladata.trackEvent('page_load', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'page_url', '');
                }else {
                    ga('send', 'event', 'desktop', 'page_url', '');
                }


                var queryResult = document.querySelector('.x-content');
                angular.element(queryResult).addClass('openMail');

                $timeout(function() {
                    angular.element(queryResult).addClass('showPhoto');
                }, 3000);
            }else if(newTab === 3){
                cooladata.trackEvent('sharing_button', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
                cooladata.trackEvent('page_load', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'sharing_button', '');
                    ga('send', 'event', 'mobile', 'page_url', '');
                }else{
                    ga('send', 'event', 'desktop', 'sharing_button', '');
                    ga('send', 'event', 'desktop', 'page_url', '');
                }


                ezfb.ui(
                    {
                        method: 'feed',
                        link: 'http://reshet.tv/mood/shows-the-amazing-race-launch-game/?utm_source=facebook&utm_medium=post-shared-game&utm_content=launching-game&utm_campaign=launching-game',
                        picture: "http://www.slash.co.il/projects/reshet/hamerotz_hashaka/api/?method=get_share_image&v=1&uid1=" + $scope.user.uid + "&uid2=" + $scope.user_friend.uid,
                        name: 'אני ו-'+$filter('firstWord')($scope.user_friend.name)+' הזוג המושלם לנצח במירוץ!',
                        description: $scope.sharedText,
                        caption: 'הזוג המושלם לנצח במירוץ'
                    },
                    function (res) {
                        // res: FB.ui response
                    }
                );


                if(window.innerWidth <= 768){
                    angular.element(document.querySelector('.x-content-left-mail')).css({
                        transition: 'all 500ms linear',
                        opacity: '0'
                    });
                }
            }
        };
        $scope.isTab = function (tab) {
            return $scope.tab === tab;
        };




        /**
         * Facebook login
         */
        updateLoginStatus(updateApiMe);

        $scope.login = function () {
            cooladata.trackEvent('login_button', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
                ga('send', 'event', 'mobile', 'login_button', '');
            } else {
                ga('send', 'event', 'desktop', 'login_button', '');
            }

            // fix iOS Chrome
            if( navigator.userAgent.match('CriOS') ) {
                window.location.href = 'https://www.facebook.com/dialog/oauth?client_id=1788004364764805&redirect_uri=slash.co.il/projects/reshet/hamerotz_hashaka/app/2.0/&scope=user_photos,user_posts,user_friends';
            }else{
                ezfb.login(function (res) {
                    if (res.authResponse) {
                        updateLoginStatus(updateApiMe);
                        $scope.logged = true;
                        $scope.userToken = res.authResponse.accessToken;
                        //console.log($scope.userToken);
                        $scope.getMeAndFriend();
                    }
                }, {scope: 'email,user_photos,user_posts,user_friends,public_profile,user_location'});
            }
        };



        $scope.logout = function () {
            ezfb.logout(function () {
                updateLoginStatus(updateApiMe);
            });
        };

        /**
         * Update loginStatus result
         */
        function updateLoginStatus (more) {
            ezfb.getLoginStatus(function (res) {
                console.log(res);
                $scope.loginStatus = res;

                (more || angular.noop)();
            });



            // fix iOS Chrome
            if( navigator.userAgent.match('CriOS') ) {
                ezfb.getLoginStatus(function (res) {
                    if(res.status === 'connected') {
                        $scope.loginStatus = res;

                        $scope.logged = true;
                        $scope.userToken = res.authResponse.accessToken;
                        $scope.getMeAndFriend();

                        (more || angular.noop)();
                    }
                });
            }
        }

        /**
         * Update api('/me') result
         */
        function updateApiMe () {
            ezfb.api('/me', {fields: 'id,name,gender,location,email'}, function (res) {
                console.log(res);
                //$scope.user = res;
                $scope.userData = {
                    gender: res.gender ? res.gender : '',
                    email: res.email ? res.email : '',
                    location: {
                        name: res.location ? res.location.name : ''
                    }
                };
            });
        }






        /**
         * getMeandFriend
         */
        $scope.getMeAndFriend = function() {
            $http.get("../../api/?method=get_friend&access_token=" + $scope.userToken).success(function(data, status) {
                if('error' in data.result){
                    $window.location.reload();
                    return;
                }
                //console.log(data);
                $scope.user = data.result.me;
                $scope.user_friend = data.result.friend;

                $timeout(function() {
                    $scope.setTab(2);
                }, 1000);
            });
        };






        /**
         * send data form forms
        */
        $scope.fullname = '';
        $scope.phone = '';
        $scope.email = '';
        $scope.sharedText = '';
        $scope.sharedTextCheckBox = false;
        $scope.terms = false;
        $scope.sendInputData = function($event){
            cooladata.trackEvent('send_button', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                ga('send', 'event', 'mobile', 'send_button', '');
            }else {
                ga('send', 'event', 'desktop', 'send_button', '');
            }

            var sendData = {
                fullname: $scope.fullname,
                phone: $scope.phone,
                email: $scope.email,
                text: $scope.sharedText,
                approve_mail: $scope.sharedTextCheckBox ? 1:0,
                fb_user_uid: $scope.user.uid,
                fb_user_name: $scope.user.name,
                fb_friend_uid: $scope.user_friend.uid,
                fb_friend_name: $scope.user_friend.name,

                lead_fb_user_email: $scope.userData.email,
                lead_fb_user_gender: $scope.userData.gender,
                lead_fb_user_location: $scope.userData.location.name
            };
            //lead_fb_user_phone - user's phone from Facebook
            //lead_fb_user_location - user's address (city + country) from Facebook

            angular.element($event.currentTarget).addClass('send');
            $timeout(function(){
                window.top.location = 'http://reshet.tv/mood/shows-the-amazing-race/';
            },3000);

            console.log(sendData);

            $http.post("../../api/?method=send_data" +
                "&fullname=" + encodeURIComponent(sendData.fullname) +
                "&phone=" + encodeURIComponent(sendData.phone) +
                "&email=" + encodeURIComponent(sendData.email) +
                "&text=" + encodeURIComponent(sendData.text) +
                "&approve_mail=" + encodeURIComponent(sendData.approve_mail) +
                "&fb_user_uid=" + encodeURIComponent(sendData.fb_user_uid) +
                "&fb_user_name=" + encodeURIComponent(sendData.fb_user_name) +
                "&fb_friend_uid=" + encodeURIComponent(sendData.fb_friend_uid) +
                "&fb_friend_name=" + encodeURIComponent(sendData.fb_friend_name) +
                "&lead_fb_user_email=" + encodeURIComponent(sendData.lead_fb_user_email) +
                "&lead_fb_user_gender=" + encodeURIComponent(sendData.lead_fb_user_gender) +
                "&lead_fb_user_location=" + encodeURIComponent(sendData.lead_fb_user_location)).success(function(data, status) {})
        };




        /**
         * trackEvents
         */
        $scope.checkTypingStory = function() {
            var sharedTextLength = $scope.sharedText ? $scope.sharedText.length : 0;
            if($scope.sharedText != '' && sharedTextLength >= 1 && sharedTextLength < 2){
                cooladata.trackEvent('typing_story', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});

                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'typing_story', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_story', '');
                }
            }
        };

        $scope.checkTypingInputs = function() {
            var fullnameLength = $scope.fullname ? $scope.fullname.length : 0;
            var phoneLength = $scope.phone ? $scope.phone.length : 0;
            var emailLength = $scope.email ? $scope.email.length : 0;
            if($scope.fullname != '' && fullnameLength >= 1 && fullnameLength < 2){
                cooladata.trackEvent('typing_details_form', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});

                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'typing_details_form', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_details_form', '');
                }
            }
            else if($scope.phone != '' && phoneLength >= 1 && phoneLength < 2){
                cooladata.trackEvent('typing_details_form', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});

                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'typing_details_form', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_details_form', '');
                }
            }
            else if($scope.email != '' && emailLength >= 1 && emailLength < 2){
                cooladata.trackEvent('typing_details_form', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});

                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'typing_details_form', '');
                }else {
                    ga('send', 'event', 'desktop', 'typing_details_form', '');
                }
            }
        };

        $scope.acceptTermsButton = function(){
            if($scope.terms){
                //console.log(1);
                cooladata.trackEvent('accept_terms_button', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                    ga('send', 'event', 'mobile', 'accept_terms_button', '');
                }else {
                    ga('send', 'event', 'desktop', 'accept_terms_button', '');
                }
            }
        };

        cooladata.trackEvent('page_load', {'user_alternative_id': $scope.user.uid, 'page_url': 'http://www.slash.co.il/projects/reshet/hamerotz_hashaka/app/'});
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            ga('send', 'event', 'mobile', 'page_url', '');
        }else {
            ga('send', 'event', 'desktop', 'page_url', '');
        }
    }
]);


app.directive('preLoader', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            angular.element(document).ready(function () {
                $timeout(function () {
                    element.addClass('hide');
                    angular.element(document.querySelector('.app')).removeClass('ovh');
                }, 500);
            });
        }
    };
});

















































//http://www.slash.co.il/projects/reshet/hamerotz_hashaka/api/?method=send_data&fullname=123123&phone=052-1234561&email=a@asd.asd&text=lasttest&approve_mail=1&fb_user_uid=1001140479966144&fb_user_name=Dima Lutsik&fb_friend_uid=987937617948461&fb_friend_name=Natalie Dovkin&lead_fb_user_email=di3orlive@yahoo.com&lead_fb_user_gender=male&lead_fb_user_location=Lviv, Ukraine


















